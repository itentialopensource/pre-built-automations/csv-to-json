<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 01-15-2024 and will be end of life on 01-15-2025. The capabilities of this Pre-Built have been replaced by the [IAP - Data Manipulation](https://gitlab.com/itentialopensource/pre-built-automations/iap-data-manipulation)

# CSV to JSON

## Table of Contents
* [Overview](#overview)
* [Installation Prerequisites](#installation-prerequisites)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)

## Overview
This JST allows IAP users to convert a comma separated file (.csv) into a JSON file. See [How to Run](#how-to-run) for more details.

## Installation Prerequisites
Users must satisfy the following prerequisites:
<!-- Include any other required apps or adapters in this list -->
<!-- Ex.: EC2 Adapter -->
* Itential Automation Platform
 * `^2022.1`
 
## How to Install
To install the artifact:
* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Prerequisites](#installation-prerequisites) section. 
* The artifact can be installed from within `App-Admin_Essential`. Simply search for the name of your desired artifact and click the install button.

## How to Run
Use the following to run the artifact:
1. Once the JST is installed as outlined in the [How to Install](#how-to-install) section above, navigate to the workflow where you would like to convert comma separated data into JSON data and add a `JSON Transformation` task. 

2. Inside the `Transformation` task, search for and select  `csvToJson` (the name of the internal JST).
 
3. The input to the JST is the comma separated string that needs to be converted. 

4. Save your input and the task is ready to run inside of IAP.

## Important Notes
- This transformation only converts comma separated data with headers. 
- It is **always** assumed that the first row of the string (anything before the first linebreak) contains the headers for the mapping. 
- The transformation does not support escaped commas within the data/header. For example: `abc,def",",efg` will be treated as `[ 'abc', 'def"', '"', 'efg' ]` rather than `[ 'abc', 'def,' , 'efg' ]`.
