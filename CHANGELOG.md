
## 0.0.9 [01-30-2024]

* fix list numbers

See merge request itentialopensource/pre-built-automations/csv-to-json!8

---

## 0.0.8 [01-30-2024]

* make changes for deprecation

See merge request itentialopensource/pre-built-automations/csv-to-json!7

---

## 0.0.7 [05-24-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/csv-to-json!6

---

## 0.0.6 [06-06-2022]

* Certified on 2022.1

See merge request itentialopensource/pre-built-automations/csv-to-json!5

---

## 0.0.5 [12-03-2021]

* Certify on 2021.2

See merge request itentialopensource/pre-built-automations/csv-to-json!4

---

## 0.0.4 [07-02-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/csv-to-json!3

---

## 0.0.3 [06-11-2021]

* Certify on IAP 2021.1

See merge request itentialopensource/pre-built-automations/csv-to-json!2

---

## 0.0.2 [12-23-2020]

* [LB-306] patch/README.md

See merge request itentialopensource/pre-built-automations/staging/csv-to-json!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n\n
